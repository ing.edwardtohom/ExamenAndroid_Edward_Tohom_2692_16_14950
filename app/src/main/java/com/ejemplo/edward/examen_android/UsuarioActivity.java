package com.ejemplo.edward.examen_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by Edward on 29/07/2017.
 */

public class UsuarioActivity extends AppCompatActivity {

    private Button bt1;
    private TextView tv1;
    private RadioButton radioButton1, radioButton3, radioButton5, radioButton2, radioButton4, radioButton6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        radioButton1=(RadioButton) findViewById(R.id.radioButton1);
        radioButton3=(RadioButton) findViewById(R.id.radioButton3);
        radioButton5=(RadioButton) findViewById(R.id.radioButton5);
        radioButton2=(RadioButton) findViewById(R.id.radioButton2);
        radioButton4=(RadioButton) findViewById(R.id.radioButton4);
        radioButton6=(RadioButton) findViewById(R.id.radioButton6);
        bt1=(Button) findViewById(R.id.ok);
        tv1=(TextView) findViewById(R.id.tv1);
    }

    public void agregar(View view){//metodo para sumar numeros


        if (radioButton1.isChecked() && radioButton3.isChecked() && radioButton5.isChecked() ) {
            tv1.setText(String.valueOf("Usted le gusta Android, Java, Spring!"));

        }
        else
        if (radioButton1.isChecked() && radioButton3.isChecked()) {
            tv1.setText(String.valueOf("Usted le gusta Android, Java!"));

        }
        else
        if (radioButton3.isChecked() && radioButton5.isChecked()) {
            tv1.setText(String.valueOf("Usted le gusta Java y Spring!"));

        }
        else
        if (radioButton1.isChecked() && radioButton5.isChecked()) {
            tv1.setText(String.valueOf("Usted le gusta Android y Spring!"));

        }
        else
        if (radioButton1.isChecked()) {
            tv1.setText(String.valueOf("Usted solo le gusta Android!"));

        }
        else
        if (radioButton3.isChecked()) {
            tv1.setText(String.valueOf("Usted solo le gusta Java!"));

        }
        else
        if (radioButton5.isChecked()) {
            tv1.setText(String.valueOf("Usted solo le gusta Spring!"));

        }
        else
        if (radioButton2.isChecked() && radioButton4.isChecked() && radioButton6.isChecked() ) {
            tv1.setText(String.valueOf("Usted NO le gusta NINGUN LENGUAJE!"));
        }



    }

}
