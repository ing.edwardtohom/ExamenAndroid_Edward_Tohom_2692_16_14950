package com.ejemplo.edward.examen_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edt1, edt2, edt3, edt4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt1 = (EditText) findViewById(R.id.edt1);
        edt2 = (EditText) findViewById(R.id.edt2);
        edt3 = (EditText) findViewById(R.id.edt3);
        edt4 = (EditText) findViewById(R.id.edt4);

    }
    public void login(View view) {

        String user = edt3.getText().toString();
        String pass = edt4.getText().toString();


       if (user.endsWith("123") && pass.equals("123"))
        //if (user.equals(pass))
        {
            Intent i = new Intent(this, UsuarioActivity.class);
            startActivity(i);
        }
        else {
            Toast notification = Toast.makeText(this, "Credenciales invalidas ", Toast.LENGTH_SHORT);
            notification.show();
        }
    }
    public void limpiar(View view) {
        edt1.setText("");
        edt2.setText("");
        edt3.setText("");
        edt4.setText("");


    }

}
